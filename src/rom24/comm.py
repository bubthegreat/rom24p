"""Here's all your comms packages!"""

import random
import time
import logging
from collections import OrderedDict
from types import MethodType
from rom24 import db
from rom24 import game_utils
from rom24 import handler_game
from rom24 import merc
from rom24 import nanny
from rom24 import handler_ch
from rom24 import state_checks
from rom24 import instance

LOGGER = logging.getLogger(__name__)

def process_input() -> None:
    """Process the user input."""
    for descriptor in merc.descriptor_list:
        if descriptor.active and descriptor.cmd_ready and descriptor.connected:
            descriptor.connected()
            if descriptor.is_connected(nanny.con_playing):
                char = handler_ch.CH(descriptor)
                if char:
                    char.timer = 0


def set_connected(self, state) -> None:
    """Set the connected state."""
    self.connected = MethodType(state, self)


def is_connected(self, state) -> bool:
    """Return the connected state."""
    return self.connected == MethodType(state, self)


def process_output(self) -> None:
    """Process any user output."""
    # pylint: disable=too-many-branches
    char = handler_ch.CH(self)
    if char and self.is_connected(nanny.con_playing) and self.send_buffer:
        # /* battle prompt */
        if char.fighting:
            victim = char.fighting
            if victim and char.can_see(victim):
                if victim.max_hit > 0:
                    percent = victim.hit * 100 / victim.max_hit
                else:
                    percent = -1
                if percent >= 100:
                    wound = "is in excellent condition."
                elif percent >= 90:
                    wound = "has a few scratches."
                elif percent >= 75:
                    wound = "has some small wounds and bruises."
                elif percent >= 50:
                    wound = "has quite a few wounds."
                elif percent >= 30:
                    wound = "has some big nasty wounds and scratches."
                elif percent >= 15:
                    wound = "looks pretty hurt."
                elif percent >= 0:
                    wound = "is in awful condition."
                else:
                    wound = "is bleeding to death."
                wound = f"{state_checks.PERS(victim, char)} {wound} \n"
                wound = wound.capitalize()
                char.send(wound)
        if not char.comm.is_set(merc.COMM_COMPACT):
            self.send("\n")
        bust_a_prompt(char)
    self.miniboa_send()


def init_descriptor(descriptor) -> None:
    """Initialize a descriptor for a user."""
    descriptor.set_connected = MethodType(set_connected, descriptor)
    descriptor.is_connected = MethodType(is_connected, descriptor)
    descriptor.set_connected(nanny.con_get_name)
    greeting = random.choice(merc.greeting_list)
    descriptor.send(greeting.text)
    descriptor.active = True
    descriptor.character = None
    descriptor.original = None
    descriptor.snoop_by = None
    descriptor.close = descriptor.deactivate
    # Gain control over process output without messing with miniboa.
    descriptor.miniboa_send = descriptor.socket_send
    descriptor.socket_send = MethodType(process_output, descriptor)
    merc.descriptor_list.append(descriptor)
    descriptor.request_terminal_type()
    descriptor.request_naws()


def check_playing(ch_dummy, name: str) -> bool:
    """Check if a user is already playing."""
    for dold in merc.descriptor_list:
        if (
            dold != ch_dummy
            and dold.character
            and dold.connected != nanny.con_get_name(ch_dummy)
            and dold.connected != nanny.con_get_old_password(ch_dummy)
            and name == (dold.original.name if dold.original else dold.character.name)
        ):
            ch_dummy.send("That character is already playing.\n")
            ch_dummy.send("Do you wish to connect anyway (Y/N)?")
            ch_dummy.set_connected(nanny.con_break_connect(ch_dummy))
            return True
    return False


def check_reconnect(ch_dummy, f_conn) -> bool:
    """Look for link-dead player to reconnect."""
    for char in instance.players.values():
        if not char.is_npc() and (not f_conn or not char.desc) and ch_dummy.character.name == char.name:  # pylint: disable=line-too-long
            if not f_conn:
                ch_dummy.character.pwd = char.pwd
            else:
                ch_dummy.character.pwd = ""
                del ch_dummy.character
                ch_dummy.character = char
                char.desc = ch_dummy
                char.timer = 0
                char.send("Reconnecting. Type replay to see missed tells.\n")
                handler_game.act("$n has reconnected.", char, None, None, merc.TO_ROOM)
                LOGGER.info("%s@%s reconnected.", char.name, ch_dummy.host)
                handler_game.wiznet("$N groks the fullness of $S link.", char, None, merc.WIZ_LINKS, 0, 0)  # pylint: disable=line-too-long
                ch_dummy.set_connected(nanny.con_playing)
            return True
    return False


def close_socket(descriptor) -> None:
    """Close the socket for a player."""
    if descriptor in merc.descriptor_list:
        merc.descriptor_list.remove(descriptor)
    descriptor.active = False


def bust_a_prompt(char) -> None:
    """Set player settable prompt."""
    # pylint: disable=too-many-branches
    # pylint: disable=too-many-statements
    # pylint: disable=too-many-boolean-expressions
    dir_name = ["N", "E", "S", "W", "U", "D"]
    room = char.in_room
    doors = ""
    pstr = char.prompt
    if not pstr:
        char.send(f"<{char.hit}hp {char.mana}m {char.move}mv> {char.prefix}")
        return
    if char.comm.is_set(merc.COMM_AFK):
        char.send("<AFK> ")
        return
    replace = OrderedDict()
    found = False
    for door, pexit in enumerate(room.exit):
        if (
            pexit
            and (
                pexit.to_room
                and char.can_see_room(pexit.to_room)
                or (char.is_affected(merc.AFF_INFRARED) and not char.is_affected(merc.AFF_BLIND))
            )
            and not pexit.exit_info.is_set(merc.EX_CLOSED)
        ):
            found = True
            doors += dir_name[door]
    if not found:
        replace["%e"] = "none"
    else:
        replace["%e"] = doors
    replace["%c"] = "\n"
    replace["%h"] = f"{char.hit}"
    replace["%H"] = f"{char.max_hit}"
    replace["%m"] = f"{char.mana}"
    replace["%M"] = f"{char.max_mana}"
    replace["%v"] = f"{char.move}"
    replace["%V"] = f"{char.max_move}"
    replace["%x"] = f"{char.exp}"
    replace["%X"] = f"{(0 if char.is_npc() else (char.level + 1) * char.exp_per_level(char.points) - char.exp)}"  # pylint: disable=line-too-long
    replace["%g"] = f"{char.gold}"
    replace["%s"] = f"{char.silver}"
    if char.level > 9:
        replace["%a"] = f"{char.alignment}"
    else:
        align = "good" if char.is_good() else "evil" if char.is_evil() else "neutral"
        replace["%a"] = f"{align}"

    if char.in_room:
        if (not char.is_npc() and char.act.is_set(merc.PLR_HOLYLIGHT)) or (
            not char.is_affected(merc.AFF_BLIND) and not char.in_room.is_dark()
        ):
            replace["%r"] = char.in_room.name
        else:
            replace["%r"] = "darkness"
    else:
        replace["%r"] = " "

    if char.is_immortal() and char.in_room:
        replace["%R"] = f"{char.in_room.vnum}"
    else:
        replace["%R"] = " "

    if char.is_immortal() and char.in_room:
        replace["%z"] = f"{instance.area_templates[char.in_room.area].name}"
    else:
        replace["%z"] = " "

    # replace['%%'] = '%'
    prompt = char.prompt
    prompt = game_utils.mass_replace(prompt, replace)

    char.send(prompt)
    if char.prefix:
        char.send(char.prefix)

    char.desc.send_ga()


def is_reconnecting(name) -> bool:
    """Check if char is reconnecting."""
    for char in instance.players.values():
        if not char.desc and char.name == name:
            return True
    return False


def game_loop(server) -> None:
    """Run the game loop."""

    from rom24.update import update_handler
    from rom24.pyom import startup_time
    
    db.boot_db()

    boot_time = time.time()
    # boot_snapshot = sysutils.ResourceSnapshot()
    # LOGGER.info(boot_snapshot.log_data())

    LOGGER.info("Pyom database booted in %.3f seconds", (boot_time - startup_time))
    LOGGER.info("Saving instances")
    # instance.save()
    LOGGER.info("Beginning server polling.")
    LOGGER.info("Pyom is ready to rock on port %d", server.port)

    errors = 0
    connection_done = False
    while not connection_done:
        try:
            server.poll()
            process_input()
            update_handler()
        except Exception as err:  # pylint: disable=broad-exception-caught
            LOGGER.exception("Exception %s caught - continuing loop.", err)
            # Protect us from infinite errors.
            errors += 1
            if errors > 50:
                connection_done = True
