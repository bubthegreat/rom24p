import logging
import re

logger = logging.getLogger(__name__)

from rom24 import merc
from rom24 import interp
from rom24 import instance

AREA_REGEX = re.compile(r"{(?P<min_level>\d+)\s+(?P<max_level>\d+)}\s+(?P<author>\w+)\s+(?P<area>.*)")
BLINE = "---------------------------------------------------\n"


def do_areas(ch, argument):
    if argument:
        ch.send("No argument is used with this command.\n")
        return
    area_creds = []
    for iArea in instance.areas.values():
        match = AREA_REGEX.search(iArea.credits)
        if not match:
            logger.exception("Error parsing %s", iArea.credits)
            continue
        creds_dict = match.groupdict()
        # looks like: {'min_level': '51', 'max_level': '60', 'author': 'ROM', 'area': 'Immortal Zone'}

        min_level = int(creds_dict["min_level"])
        max_level = int(creds_dict["max_level"])
        author = creds_dict["author"]
        area = creds_dict["area"]
        area_creds.append((min_level, max_level, author, area))

    ch.send(f"{'Range':<10}{'Author':<13}{'Area':<10}\n")
    ch.send(BLINE)
    for min_lev, max_lev, author_handle, area_name in sorted(area_creds):
        lev_str = f"{min_lev}-{max_lev}"
        ch.send(f"{lev_str:<10}{author_handle:<13}{area_name:<10}\n")


interp.register_command(interp.cmd_type("areas", do_areas, merc.POS_DEAD, 0, merc.LOG_NORMAL, 1))
