import time
import logging

logger = logging.getLogger(__name__)

from rom24 import state_checks
from rom24 import merc
from rom24 import interp

BLINE = "-------------------------------------------------------------------------------\n"
EMPTY = "\n"
pad1 = 10
pad2 = 13


def get_position(ch):
    # Move this into ch.position as an attribute
    if ch.position == merc.POS_DEAD:
        string_position = "DEAD!!"
    elif ch.position == merc.POS_MORTAL:
        string_position = "wounded"
    elif ch.position == merc.POS_INCAP:
        string_position = "incapacitated"
    elif ch.position == merc.POS_STUNNED:
        string_position = "stunned"
    elif ch.position == merc.POS_SLEEPING:
        string_position = "sleeping"
    elif ch.position == merc.POS_RESTING:
        string_position = "resting"
    elif ch.position == merc.POS_SITTING:
        string_position = "sitting"
    elif ch.position == merc.POS_STANDING:
        string_position = "standing"
    elif ch.position == merc.POS_FIGHTING:
        string_position = "fighting"
    return string_position


def get_alignment(ch):
    if ch.alignment > 900:
        string_align = "angelic"
    elif ch.alignment > 700:
        string_align = "saintly"
    elif ch.alignment > 350:
        string_align = "good"
    elif ch.alignment > 100:
        string_align = "kind"
    elif ch.alignment > -100:
        string_align = "neutral"
    elif ch.alignment > -350:
        string_align = "mean"
    elif ch.alignment > -700:
        string_align = "evil"
    elif ch.alignment > -900:
        string_align = "demonic"
    else:
        string_align = "satanic"
    return string_align


def get_ac_str(ch, ac_type):
    ac_val = state_checks.GET_AC(ch, ac_type)

    if state_checks.GET_AC(ch, ac_type) >= 101:
        ac_str = "hopelessly vulnerable"
    elif state_checks.GET_AC(ch, ac_type) >= 80:
        ac_str = "defenseless"
    elif state_checks.GET_AC(ch, ac_type) >= 60:
        ac_str = "barely protected"
    elif state_checks.GET_AC(ch, ac_type) >= 40:
        ac_str = "slightly armored"
    elif state_checks.GET_AC(ch, ac_type) >= 20:
        ac_str = "somewhat armored"
    elif state_checks.GET_AC(ch, ac_type) >= 0:
        ac_str = "armored"
    elif state_checks.GET_AC(ch, ac_type) >= -20:
        ac_str = "well-armored"
    elif state_checks.GET_AC(ch, ac_type) >= -40:
        ac_str = "very well-armored"
    elif state_checks.GET_AC(ch, ac_type) >= -60:
        ac_str = "heavily armored"
    elif state_checks.GET_AC(ch, ac_type) >= -80:
        ac_str = "superbly armored"
    elif state_checks.GET_AC(ch, ac_type) >= -100:
        ac_str = "almost invulnerable"
    else:
        ac_str = "divinely armored"

    # TODO:  Find a better way to mask this without doing the work first
    if ch.level < 25:
        ac_val = "??"
        ac_str = ""

    return f"{ac_val} {ac_str}"


def do_score(ch, argument):
    # Set values
    ptitle = f"{ch.name}{'' if ch.is_npc() else ch.title}"
    gender = "sexless" if ch.sex == 0 else "male" if ch.sex == 1 else "female"
    pclass = "mobile" if ch.is_npc() else ch.guild.name
    age_str = f"{ch.get_age()} years old ({(ch.played + (int)(time.time() - ch.logon)) // 3600} hours)"
    carry_str = f"{ch.carry_number} / {ch.can_carry_n()}"
    weight_str = f"{state_checks.get_carry_weight(ch) // 10} / {ch.can_carry_w() // 10} lbs"
    hp_str = f"{ch.hit}/{ch.max_hit} hp"
    mp_str = f"{ch.mana}/{ch.max_mana} mp"
    mv_str = f"{ch.move}/{ch.max_move} mv"
    str_str = f"{ch.perm_stat[merc.STAT_STR]}({ch.stat(merc.STAT_STR)})"
    int_str = f"{ch.perm_stat[merc.STAT_INT]}({ch.stat(merc.STAT_INT)})"
    wis_str = f"{ch.perm_stat[merc.STAT_WIS]}({ch.stat(merc.STAT_WIS)})"
    dex_str = f"{ch.perm_stat[merc.STAT_DEX]}({ch.stat(merc.STAT_DEX)})"
    con_str = f"{ch.perm_stat[merc.STAT_CON]}({ch.stat(merc.STAT_CON)})"
    hr_str = f"{ state_checks.GET_HITROLL(ch) if ch.level >= 15 else '??'}"
    dr_str = f"{ state_checks.GET_DAMROLL(ch) if ch.level >= 15 else '??'}"

    # States
    pstates = []
    if not ch.is_npc() and ch.condition[merc.COND_DRUNK] > 10:
        pstates.append("You are drunk.")
    if not ch.is_npc() and ch.condition[merc.COND_THIRST] == 0:
        pstates.append("You are thirsty.")
    if not ch.is_npc() and ch.condition[merc.COND_HUNGER] == 0:
        pstates.append("You are hungry.")
    if ch.trust != ch.level:
        pstates.append(f"You are trusted at level {ch.trust}.")

    # Send it baby
    ch.send(f"{ptitle}\n")
    ch.send(BLINE)
    ch.send(f"{'Level':<10}: {ch.level:<13} {'Age':<10}: {age_str}\n")
    ch.send(f"{'Carry #':<10}: {carry_str:<13} {'Weight':<10}: {weight_str:<13}\n")
    ch.send(f"{'Practices':<10}: {ch.practice:<13} {'Trains':<10}: {ch.train:<13}\n")
    ch.send(
        f"{'XP':<10}: {ch.exp:<13} {'TNL':<10}: {(ch.level + 1) * ch.exp_per_level(ch.points) - ch.exp:<13} {'Alignment':<10}: {get_alignment(ch):<13}\n"
    )
    ch.send(f"{'Gold':<10}: {ch.gold:<13} {'Silver':<10}: {ch.silver:<13} {'Wimpy':<10}: {ch.wimpy} hp\n")
    ch.send(f"{'Race':<10}: {ch.race.name:<13} {'Sex':<10}: {gender:<13} {'Class':<10}: {pclass:<13}\n")
    ch.send(f"{'Hit Points':<10}: {hp_str:<13} {'Mana':<10}: {mp_str:<13} {'Moves':<10}: {mv_str:<13}\n")
    ch.send(f"{'Str':<10}: {str_str:<13} {'Int':<10}: {int_str:<13} {'Wis':<10}: {wis_str:<13}\n")
    ch.send(f"{'Dex':<10}: {dex_str:<13} {'Con':<10}: {con_str:<13}\n")
    ch.send(f"{'Hitroll':<10}: {hr_str:<13} {'Damroll':<10}: {dr_str:<13} {'Position':<10}: {get_position(ch):<13}\n")
    ch.send(
        f"{'AC Pierce':<10}: {get_ac_str(ch, merc.AC_PIERCE):<23} {'AC Bash':<10}: {get_ac_str(ch, merc.AC_BASH):<13}\n"
    )
    ch.send(
        f"{'AC Slash':<10}: {get_ac_str(ch, merc.AC_SLASH):<23} {'AC Exotic':<10}: {get_ac_str(ch, merc.AC_EXOTIC):<13}\n"
    )
    ch.send(BLINE)

    if ch.is_immortal():
        ch.send(f"{'Holy Light':<10}: {'on' if ch.act.is_set(merc.PLR_HOLYLIGHT) else 'off'}\n")
        ch.send(f"{'Invisible':<10}: {ch.invis_level}\n")
        ch.send(f"{'Incognito':<10}: {ch.incog_level}\n")

    for pstate in pstates:
        ch.send(f"{pstate}\n")
    ch.send(BLINE)
    ch.send("You are affected by:\n")
    ch.do_affects("")


interp.register_command(interp.cmd_type("score", do_score, merc.POS_DEAD, 0, merc.LOG_NORMAL, 1))
