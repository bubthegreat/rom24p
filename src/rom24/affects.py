""" Module covering all affects.

This baby has all the affects - and of course, none of them are well defined in objects!


"""
import copy
import json
import logging
from typing import Any

from rom24 import bit
from rom24 import const
from rom24 import handler_game
from rom24 import merc
from rom24 import tables
from rom24 import instance

LOGGER = logging.getLogger(__name__)

class Affects:
    """General affects class that has no actual attributes."""
    # pylint: disable=no-member

    def __init__(self, **kwargs):
        super().__init__()
        self.affected = []
        self.affected_by = bit.Bit(flags=tables.affect_flags)
        if kwargs:
            for key, val in kwargs.items():
                setattr(self, key, copy.deepcopy(val))

    def to_json(self, outer_encoder=None):
        """Convert affect class to json."""

        if outer_encoder is None:
            outer_encoder = json.JSONEncoder.default

        tmp_dict = {}
        for key, val in self.__dict__.items():
            if str(type(val)) in ("<class 'function'>", "<class 'method'>"):
                continue
            tmp_dict[key] = val

        cls_name = "__class__/" + __name__ + "." + self.__class__.__name__
        return {cls_name: outer_encoder(tmp_dict)}

    @classmethod
    def from_json(cls, data, outer_decoder=None) -> Any:
        """Load an affect from a json value."""
        if outer_decoder is None:
            outer_decoder = json.JSONDecoder.decode

        cls_name = "__class__/" + __name__ + "." + cls.__name__
        if cls_name in data:
            tmp_data = outer_decoder(data[cls_name])
            return cls(**tmp_data)
        return data

    def is_affected(self, aff) -> bool:
        """Check if affected by affect."""
        if isinstance(aff, const.skill_type):
            aff = aff.name
        if isinstance(aff, str):
            is_affected = bool([paf for paf in self.affected if paf.type == aff][:1])
            return is_affected
        is_affected = self.affected_by.is_set(aff)
        return is_affected

    def affect_add(self, paf) -> None:
        """Add an affect to the affects class instance."""
        paf_new = handler_game.AFFECT_DATA()
        paf_new.__dict__ = paf.__dict__.copy()
        self.affected.append(paf_new)
        self.affect_modify(paf_new, True)

    def affect_join(self, paf) -> None:
        """Add or enhance an affect."""
        for paf_old in self.affected:
            if paf_old.type == paf.type:
                paf.level = (paf.level + paf_old.level) // 2
                paf.duration += paf_old.duration
                paf.modifier += paf_old.modifier
                self.affect_remove(paf_old)
                break

        self.affect_add(paf)

    def affect_remove(self, paf) -> None:
        """Remove an affect from a char."""
        if not self.affected:
            LOGGER.error("BUG: Affect_remove: no affect.")
            return

        self.affect_modify(paf, False)
        where = paf.where
        vector = paf.bitvector

        if paf not in self.affected:
            LOGGER.error("Affect_remove: cannot find paf.")
            return

        self.affected.remove(paf)
        del paf
        self.affect_check(where, vector)

    def affect_check(self, where, vector) -> None:
        """Check if an affect is affecting a location/vector?  I dunno."""
        if where in [merc.TO_OBJECT, merc.TO_WEAPON] or not vector:
            return

        for paf in self.affected:
            if paf.where == where and paf.bitvector == vector:
                if where == merc.TO_AFFECTS:
                    self.affected_by.set_bit(vector)
                elif where == merc.TO_IMMUNE:
                    self.imm_flags.set_bit(vector)  # type: ignore
                elif where == merc.TO_RESIST:
                    self.res_flags.set_bit(vector)  # type: ignore
                elif where == merc.TO_VULN:
                    self.vuln_flags.set_bit(vector)  # type: ignore
                return

        for item_id in self.inventory[:]:  # type: ignore
            item = instance.items[item_id]
            if not item.equipped_to:
                continue
            for paf in item.affected:
                if paf.where == where and paf.bitvector == vector:
                    if where == merc.TO_AFFECTS:
                        self.affected_by.set_bit(vector)
                    elif where == merc.TO_IMMUNE:
                        self.imm_flags.set_bit(vector)  # type: ignore
                    elif where == merc.TO_RESIST:
                        self.res_flags.set_bit(vector)  # type: ignore
                    elif where == merc.TO_VULN:
                        self.vuln_flags.set_bit(vector)  # type: ignore
                    return
            if item.enchanted:
                continue
            for paf in instance.item_templates[item.vnum].affected:
                if paf.where == where and paf.bitvector == vector:
                    if where == merc.TO_AFFECTS:
                        self.affected_by.set_bit(vector)
                    elif where == merc.TO_IMMUNE:
                        self.imm_flags.set_bit(vector)  # type: ignore
                    elif where == merc.TO_RESIST:
                        self.res_flags.set_bit(vector)  # type: ignore
                    elif where == merc.TO_VULN:
                        self.vuln_flags.set_bit(vector)  # type: ignore
                    return

    # * Apply or remove an affect to a character.
    def affect_modify(self, paf, f_add):
        """Modify the affects with a modifier."""
        mod = paf.modifier
        if f_add:
            if paf.where == merc.TO_AFFECTS:
                self.affected_by.set_bit(paf.bitvector)
            elif paf.where == merc.TO_IMMUNE:
                self.imm_flags.set_bit(paf.bitvector)
            elif paf.where == merc.TO_RESIST:
                self.res_flags.set_bit(paf.bitvector)
            elif paf.where == merc.TO_VULN:
                self.vuln_flags.set_bit(paf.bitvector)
        else:
            if paf.where == merc.TO_AFFECTS:
                self.affected_by.rem_bit(paf.bitvector)
            elif paf.where == merc.TO_IMMUNE:
                self.imm_flags.rem_bit(paf.bitvector)
            elif paf.where == merc.TO_RESIST:
                self.res_flags.rem_bit(paf.bitvector)
            elif paf.where == merc.TO_VULN:
                self.vuln_flags.rem_bit(paf.bitvector)
            mod = 0 - mod

        if paf.location == merc.APPLY_NONE:
            pass
        elif paf.location == merc.APPLY_STR:
            self.mod_stat[merc.STAT_STR] += mod
        elif paf.location == merc.APPLY_DEX:
            self.mod_stat[merc.STAT_DEX] += mod
        elif paf.location == merc.APPLY_INT:
            self.mod_stat[merc.STAT_INT] += mod
        elif paf.location == merc.APPLY_WIS:
            self.mod_stat[merc.STAT_WIS] += mod
        elif paf.location == merc.APPLY_CON:
            self.mod_stat[merc.STAT_CON] += mod
        elif paf.location == merc.APPLY_SEX:
            self.sex += mod
        elif paf.location == merc.APPLY_CLASS:
            pass
        elif paf.location == merc.APPLY_LEVEL:
            pass
        elif paf.location == merc.APPLY_AGE:
            pass
        elif paf.location == merc.APPLY_HEIGHT:
            pass
        elif paf.location == merc.APPLY_WEIGHT:
            pass
        elif paf.location == merc.APPLY_MANA:
            self.max_mana += mod
        elif paf.location == merc.APPLY_HIT:
            self.max_hit += mod
        elif paf.location == merc.APPLY_MOVE:
            self.max_move += mod
        elif paf.location == merc.APPLY_GOLD:
            pass
        elif paf.location == merc.APPLY_EXP:
            pass
        elif paf.location == merc.APPLY_AC:
            for i in range(4):
                self.armor[i] += mod
        elif paf.location == merc.APPLY_HITROLL:
            self.hitroll += mod
        elif paf.location == merc.APPLY_DAMROLL:
            self.damroll += mod
        elif paf.location == merc.APPLY_SAVES:
            self.saving_throw += mod
        elif paf.location == merc.APPLY_SAVING_ROD:
            self.saving_throw += mod
        elif paf.location == merc.APPLY_SAVING_PETRI:
            self.saving_throw += mod
        elif paf.location == merc.APPLY_SAVING_BREATH:
            self.saving_throw += mod
        elif paf.location == merc.APPLY_SAVING_SPELL:
            self.saving_throw += mod
        elif paf.location == merc.APPLY_SPELL_AFFECT:
            pass
        else:
            LOGGER.error("Affect_modify: unknown location %d.", paf.location)
            return
        #
        # * Check for weapon wielding.
        # * Guard against recursion (for weapons with affects).

        depth = 0
        wield = self.slots.main_hand
        if not self.is_npc() and wield and wield.get_weight() > (const.str_app[self.stat(merc.STAT_STR)].wield * 10):
            if depth == 0:
                depth += 1
                handler_game.act("You drop $p.", self, wield, None, merc.TO_CHAR)
                handler_game.act("$n drops $p.", self, wield, None, merc.TO_ROOM)
                self.get(wield)
                self.in_room.put(wield)
                depth -= 1
        return

    # * Strip all affects of a given sn.
    def affect_strip(self, sn_aff):
        """Strip an affect"""
        for paf in self.affected[:]:
            if paf.type == sn_aff:
                self.affect_remove(paf)
