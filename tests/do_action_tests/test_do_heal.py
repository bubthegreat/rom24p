from unittest.mock import patch, MagicMock, call
import pytest

from rom24.commands.do_heal import do_heal
import logging


@pytest.fixture
def ch():
    ch = MagicMock()
    ch.in_game = True
    ch.in_room = MagicMock()
    ch.in_room.people = []
    ch.position = 1
    return ch


@pytest.fixture
def instance():
    instance = MagicMock()
    instance.characters = {}
    return instance


@pytest.fixture
def healer():
    healer = MagicMock()
    healer.is_npc.return_value = True
    healer.act.is_set.return_value = True
    return healer


@pytest.fixture
def spell():
    spell = MagicMock()
    spell.spell_fun = MagicMock()
    return spell


@pytest.fixture
def const(spell):
    const = MagicMock()
    const.skill_table = {
        "cure light": spell,
        "cure serious": spell,
        "cure critical": spell,
        "heal": spell,
        "cure blindness": spell,
        "cure disease": spell,
        "cure poison": spell,
        "remove curse": spell,
        "refresh": spell,
    }
    return const


def test_no_healer(ch, caplog):
    do_heal(ch, "")
    assert "attempted to heal without a healer in the room." in caplog.text


def test_bad_heal_option(ch, caplog):

    # Create a healer object and add it to the room
    healer = MagicMock()
    healer.is_npc.return_value = True
    healer.act.is_set.return_value = True
    healer.id = 23
    healer.position = 1

    ch.id = 24
    ch.in_room.people = [23, 24]
    ch.position = 1

    instance = MagicMock()
    instance.characters = {23: healer, 24: ch}
    with patch("rom24.commands.do_heal.instance", instance), patch(
        "rom24.handler_game.instance", instance
    ):
        do_heal(ch, "notreal")
    assert "attempted to cast an invalid healing spell" in caplog.text


def test_display_price_list(ch, caplog):

    # Create a healer object and add it to the room
    healer = MagicMock()
    healer.is_npc.return_value = True
    healer.act.is_set.return_value = True
    healer.id = 23
    healer.position = 1

    ch.id = 24
    ch.in_room.people = [23, 24]
    ch.position = 1

    instance = MagicMock()
    instance.characters = {23: healer, 24: ch}

    # Call do_heal with no arguments
    with patch("rom24.commands.do_heal.instance", instance), patch(
        "rom24.handler_game.instance", instance
    ):
        do_heal(ch, "")
    assert ch.send.call_count == 11
