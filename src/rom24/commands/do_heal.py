import logging


logger = logging.getLogger(__name__)

from rom24 import game_utils
from rom24 import handler_game
from rom24 import state_checks
from rom24 import const
from rom24 import merc
from rom24 import interp
from rom24 import instance


def do_heal(ch, argument):
    # Find the healer in the room
    for mob_id in ch.in_room.people[:]:
        healer = instance.characters[mob_id]
        if healer.is_npc() and healer.act.is_set(merc.ACT_IS_HEALER):
            break
    else:
        ch.send("You can't do that here.\n")
        logger.warning(f"{ch.name} attempted to heal without a healer in the room.")
        return

    # Display the list of spells if no argument is given
    if not argument:
        handler_game.act("$N says 'I offer the following spells:'\n", ch, None, healer, merc.TO_CHAR)
        spells = [
            ("light:", "cure light wounds", 10),
            ("serious:", "cure serious wounds", 15),
            ("critic:", "cure critical wounds", 25),
            ("heal:", "healing spell", 50),
            ("blind:", "cure blindness", 20),
            ("disease:", "cure disease", 15),
            ("poison:", "cure poison", 25),
            ("uncurse:", "remove curse", 50),
            ("refresh:", "restore movement", 5),
            ("mana:", "restore mana", 10),
        ]
        for spell in spells:
            ch.send("{:<8} {:<25} {:<10}\n".format(*spell))
        ch.send("Type heal <type> to be healed.\n")
        logger.info(f"{ch.name} viewed the list of available spells.")
        return

    # Find the spell to cast based on the argument
    spell_data = {
        "light": ("cure light", "judicandus dies", 1000),
        "serious": ("cure serious", "judicandus gzfuajg", 1600),
        "critical": ("cure critical", "judicandus qfuhuqar", 2500),
        "heal": ("heal", "pzar", 5000),
        "blindness": ("cure blindness", "judicandus noselacri", 2000),
        "disease": ("cure disease", "judicandus eugzagz", 1500),
        "poison": ("cure poison", "judicandus sausabru", 2500),
        "uncurse": ("remove curse", "candussido judifgz", 5000),
        "mana": (None, "energizer", 1000),
        "refresh": ("refresh", "candusima", 500),
    }
    arg = argument.split()[0]
    if arg not in spell_data:
        handler_game.act(
            "$N says 'Type 'heal' for a list of spells.'",
            ch,
            None,
            healer,
            merc.TO_CHAR,
        )
        logger.warning(f"{ch.name} attempted to cast an invalid healing spell: {arg}")
        return
    spell_name, words, cost = spell_data[arg]
    if cost > (ch.gold * 100 + ch.silver):
        handler_game.act(
            "$N says 'You do not have enough gold for my services.'",
            ch,
            None,
            healer,
            merc.TO_CHAR,
        )
        logger.warning(f"{ch.name} attempted to cast {arg} but did not have enough gold.")
        return

    # Deduct the cost of the spell from the player's gold
    ch.deduct_cost(cost)

    healer.gold += cost // 100
    ch.silver -= cost % 100

    # Cast the spell
    if spell_name is None:
        # Restore mana spell
        ch.mana += game_utils.dice(2, 8) + healer.level // 3
        ch.mana = min(ch.mana, ch.max_mana)
        ch.send("A warm glow passes through you.\n")
    else:
        sn = const.skill_table[spell_name]
        spell = sn.spell_fun
        spell(sn, healer.level, healer, ch, merc.TARGET_CHAR)
        handler_game.act("$n utters the words '$T'.", healer, None, words, merc.TO_ROOM)

    state_checks.WAIT_STATE(ch, merc.PULSE_VIOLENCE)
    logger.info(f"{ch.name} cast {spell_name} and paid {cost} gold coins.")


interp.register_command(interp.cmd_type("heal", do_heal, merc.POS_RESTING, 0, merc.LOG_NORMAL, 1))
